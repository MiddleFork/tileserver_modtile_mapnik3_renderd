#Docker package for building an OSM tileserver 
## Apache, Mod_tile, mapnik3, renderd, openstreetmap_carto

## Create a volume to store the generated tile images
```docker volume create osm-tiles
```

## First Run: Download OSM Shapefiles
```
docker volume create osm-shapefiles

docker run -it -v osm-shapefiles:/data/osm/openstreetmap-carto/data -v osm-tiles:/var/lib/mod_tile middlefork/tileserver_modtile_mapnik3_renderd get-osm-shapes
```

## Production Runs:
```docker run -it -v osm-shapefiles:/data/osm/openstreetmap-carto/data -v osm-tiles:/var/lib/mod_tile middlefork/tileserver_modtile_mapnik3_renderd ```